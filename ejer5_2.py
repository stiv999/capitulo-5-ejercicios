#Autor: Steven Guaman A:            #steven.guaman@unl.edu.ec

'''
Escriba como el anterior y que muestre el maximo y minimo de los números
'''

suma = 0
t_n = 0
n_max = 0
n_min = 0
lista = []

while True:
    numero = input ('Introduzca un número >')
    if numero == 'fin' or numero == 'FIN':
        print('suma\ttotal de números\tnúm maximo\tnúm minimo')
        print(suma, '\t\t', t_n, '\t\t\t\t\t', n_max, '\t\t\t', n_min)
        break
    try:
        lista.append(numero)
        suma = suma + float(numero)
        t_n = len(lista)
        n_max = max(lista)
        n_min = min(lista)
    except:
        print ('Entrada invalida')
